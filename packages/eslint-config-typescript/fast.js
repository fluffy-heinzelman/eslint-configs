/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
let reactPluginInstalled = true;
try { require('eslint-plugin-react'); }
catch (e) { reactPluginInstalled = false; }

const commonConfig = {
    // Airbnb Eslint rules
    // -------------------------------------------------------------

    // Can be turned off for TypeScript projects because TypeScript
    // does its own lookups.
    'import/no-unresolved': 'off',

    // Rule must be disabled as it can report incorrect errors.
    // `@typescript-eslint/no-use-before-define` is used instead.
    // https://bit.ly/2RXYD9K
    'no-use-before-define': 'off',

    // @typescript-eslint/recommended
    // -------------------------------------------------------------

    // Restore default configuration like in `eslint-config-base`.
    '@typescript-eslint/no-empty-function': ['error', {
        allow: [
            'arrowFunctions',
            'functions',
            'methods'
        ]
    }],

    // Restore default configuration like in `eslint-config-base`.
    '@typescript-eslint/no-unused-vars': ['error', {
        vars: 'all',
        varsIgnorePattern: '^React$',
        args: 'after-used',
        ignoreRestSiblings: true
    }],

    // Restore default configuration like in `eslint-config-base`.
    // For the TypeScript part, error on every enum and type
    // reference before its declaration but allow stuff like:
    // interface Foo { bar: typeof bar; baz: typeof Baz.BAZ; }
    // const bar = 1;
    // class Baz { public static readonly BAZ = ''; }
    '@typescript-eslint/no-use-before-define': ['error', {
        functions: false,
        classes: true,
        variables: false,
        enums: true,
        typedefs: true,
        ignoreTypeReferences: true
    }],

    // @typescript-eslint (custom)
    // -------------------------------------------------------------

    // Allow to provide inline callbacks to any function without
    // declaring explicit return types.
    '@typescript-eslint/explicit-function-return-type': ['error', {
        allowExpressions: true
    }]
};

const tsxOverrides = {
    // React rules
    // -------------------------------------------------------------

    // Typically not using PropTypes in TypeScript projects, so only
    // error on components that have a propTypes block declared.
    // For non-React TypeScript projects or in the absence of the
    // plugin, this rule is not applied.
    ...(!reactPluginInstalled ? undefined : {
        'react/prop-types': ['error', {
            ignore: [],
            customValidators: [],
            skipUndeclared: true
        }]
    })
};

module.exports = {
    rules: {},
    overrides: [
        {
            files: ['*.ts', '*.mts', '*.cts', '*.tsx'],
            parser: '@typescript-eslint/parser',
            parserOptions: {
                sourceType: 'module'
            },
            plugins: ['@typescript-eslint'],
            extends: [
                // Disables ESLint rules already handled by TypeScript.
                'plugin:@typescript-eslint/eslint-recommended',
                // TypeScript recommended rules config.
                'plugin:@typescript-eslint/recommended'
            ],
            rules: commonConfig
        },
        {
            files: ['*.tsx'],
            rules: tsxOverrides
        }
    ]
};
