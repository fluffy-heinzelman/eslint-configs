module.exports = {
    rules: {},
    overrides: [
        {
            files: ['*.ts', '*.mts', '*.cts', '*.tsx'],
            parserOptions: {
                project: './tsconfig.json'
            },
            extends: [
                './fast',
                'plugin:@typescript-eslint/recommended-requiring-type-checking'
            ]
        }
    ]
};
