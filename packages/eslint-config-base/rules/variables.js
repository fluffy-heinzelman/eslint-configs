const { rules } = require('eslint-config-airbnb-base/rules/variables');

module.exports = {
    rules: {
        ...rules,

        // enforce or disallow variable initializations at definition
        // 'init-declarations': 'off',

        // disallow the catch clause parameter name being the same as a variable in the outer scope
        // 'no-catch-shadow': 'off',

        // disallow deletion of variables
        // @recommended
        // 'no-delete-var': 'error',

        // disallow labels that share a name with a variable
        // https://eslint.org/docs/rules/no-label-var
        // 'no-label-var': 'error',

        // disallow specific globals
        // 'no-restricted-globals': [
        //     'error',
        //     {
        //         name: 'isFinite',
        //         message: 'Use Number.isFinite instead https://github.com/airbnb/javascript#standard-library--isfinite'
        //     },
        //     {
        //         name: 'isNaN',
        //         message: 'Use Number.isNaN instead https://github.com/airbnb/javascript#standard-library--isnan'
        //     }
        // ].concat(confusingBrowserGlobals),

        // disallow declaration of variables already declared in the outer scope
        // @airbnb-delta
        'no-shadow': 'off',

        // disallow shadowing of names such as arguments
        // @recommended
        // 'no-shadow-restricted-names': 'error',

        // disallow use of undeclared variables unless mentioned in a /*global */ block
        // @recommended
        // 'no-undef': 'error',

        // disallow use of undefined when initializing variables
        // @fixable
        // 'no-undef-init': 'error',

        // disallow use of undefined variable
        // https://eslint.org/docs/rules/no-undefined
        // TODO: enable?
        // 'no-undefined': 'off',

        // disallow declaration of variables that are not used in the code
        // @recommended
        // 'no-unused-vars': ['error', {
        //     vars: 'all',
        //     args: 'after-used',
        //     ignoreRestSiblings: true
        // }],

        // disallow use of variables before they are defined
        // @airbnb-delta
        'no-use-before-define': ['error', {
            functions: false,
            classes: true,
            variables: false
        }]
    }
};
