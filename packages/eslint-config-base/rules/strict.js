const { rules } = require('eslint-config-airbnb-base/rules/strict');

module.exports = {
    rules: {
        ...rules

        // babel inserts `'use strict';` for us
        // @fixable
        // strict: ['error', 'never']
    }
};
