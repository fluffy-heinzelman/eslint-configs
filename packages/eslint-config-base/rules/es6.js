const { rules } = require('eslint-config-airbnb-base/rules/es6');

module.exports = {
    env: {
        es2018: true
    },

    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module'
    },

    rules: {
        ...rules,

        // enforces no braces where they can be omitted
        // https://eslint.org/docs/rules/arrow-body-style
        // TODO: enable requireReturnForObjectLiteral?
        // @fixable
        // @airbnb-delta
        'arrow-body-style': 'off',

        // require parens in arrow function arguments
        // https://eslint.org/docs/rules/arrow-parens
        // @fixable
        // @airbnb-delta
        'arrow-parens': 'off',

        // require space before/after arrow function's arrow
        // https://eslint.org/docs/rules/arrow-spacing
        // @fixable
        // 'arrow-spacing': ['error', { before: true, after: true }],

        // verify super() callings in constructors
        // @recommended
        // 'constructor-super': 'error',

        // enforce the spacing around the * in generator functions
        // https://eslint.org/docs/rules/generator-star-spacing
        // @fixable
        // 'generator-star-spacing': ['error', { before: false, after: true }],

        // disallow modifying variables of class declarations
        // https://eslint.org/docs/rules/no-class-assign
        // @recommended
        // 'no-class-assign': 'error',

        // disallow arrow functions where they could be confused with comparisons
        // https://eslint.org/docs/rules/no-confusing-arrow
        // @fixable
        // 'no-confusing-arrow': ['error', {
        //     allowParens: true
        // }],

        // disallow modifying variables that are declared using const
        // @recommended
        // 'no-const-assign': 'error',

        // disallow duplicate class members
        // https://eslint.org/docs/rules/no-dupe-class-members
        // @recommended
        // 'no-dupe-class-members': 'error',

        // disallow importing from the same path more than once
        // https://eslint.org/docs/rules/no-duplicate-imports
        // replaced by https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-duplicates.md
        // 'no-duplicate-imports': 'off',

        // disallow symbol constructor
        // https://eslint.org/docs/rules/no-new-symbol
        // @recommended
        // 'no-new-symbol': 'error',

        // Disallow specified names in exports
        // https://eslint.org/docs/rules/no-restricted-exports
        // @airbnb-delta
        'no-restricted-exports': 'off'

        // disallow specific imports
        // https://eslint.org/docs/rules/no-restricted-imports
        // 'no-restricted-imports': ['off', {
        //     paths: [],
        //     patterns: []
        // }],

        // disallow to use this/super before super() calling in constructors.
        // https://eslint.org/docs/rules/no-this-before-super
        // @recommended
        // 'no-this-before-super': 'error',

        // disallow useless computed property keys
        // https://eslint.org/docs/rules/no-useless-computed-key
        // @fixable
        // 'no-useless-computed-key': 'error',

        // disallow unnecessary constructor
        // https://eslint.org/docs/rules/no-useless-constructor
        // 'no-useless-constructor': 'error',

        // disallow renaming import, export, and destructured assignments to the same name
        // https://eslint.org/docs/rules/no-useless-rename
        // @fixable
        // 'no-useless-rename': ['error', {
        //     ignoreDestructuring: false,
        //     ignoreImport: false,
        //     ignoreExport: false
        // }],

        // require let or const instead of var
        // @fixable
        // 'no-var': 'error',

        // require method and property shorthand syntax for object literals
        // @fixable
        // 'object-shorthand': ['error', 'always', {
        //     ignoreConstructors: false,
        //     avoidQuotes: true
        // }],

        // suggest using arrow functions as callbacks
        // @fixable
        // 'prefer-arrow-callback': ['error', {
        //     allowNamedFunctions: false,
        //     allowUnboundThis: true
        // }],

        // suggest using of const declaration for variables that are never modified after declared
        // @fixable
        // 'prefer-const': ['error', {
        //     destructuring: 'any',
        //     ignoreReadBeforeAssign: true
        // }],

        // Prefer destructuring from arrays and objects
        // https://eslint.org/docs/rules/prefer-destructuring
        // @fixable
        // 'prefer-destructuring': ['error', {
        //     VariableDeclarator: {
        //         array: false,
        //         object: true
        //     },
        //     AssignmentExpression: {
        //         array: true,
        //         object: false
        //     },
        // }, {
        //     enforceForRenamedProperties: false
        // }],

        // disallow parseInt() in favor of binary, octal, and hexadecimal literals
        // https://eslint.org/docs/rules/prefer-numeric-literals
        // @fixable
        // 'prefer-numeric-literals': 'error',

        // suggest using Reflect methods where applicable
        // https://eslint.org/docs/rules/prefer-reflect
        // 'prefer-reflect': 'off',

        // use rest parameters instead of arguments
        // https://eslint.org/docs/rules/prefer-rest-params
        // 'prefer-rest-params': 'error',

        // suggest using the spread syntax instead of .apply()
        // https://eslint.org/docs/rules/prefer-spread
        // 'prefer-spread': 'error',

        // suggest using template literals instead of string concatenation
        // https://eslint.org/docs/rules/prefer-template
        // @fixable
        // 'prefer-template': 'error',

        // disallow generator functions that do not have yield
        // https://eslint.org/docs/rules/require-yield
        // @recommended
        // 'require-yield': 'error',

        // enforce spacing between object rest-spread
        // https://eslint.org/docs/rules/rest-spread-spacing
        // @fixable
        // 'rest-spread-spacing': ['error', 'never'],

        // import sorting
        // https://eslint.org/docs/rules/sort-imports
        // @fixable
        // 'sort-imports': ['off', {
        //     ignoreCase: false,
        //     ignoreDeclarationSort: false,
        //     ignoreMemberSort: false,
        //     memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single']
        // }],

        // require a Symbol description
        // https://eslint.org/docs/rules/symbol-description
        // 'symbol-description': 'error',

        // enforce usage of spacing in template strings
        // https://eslint.org/docs/rules/template-curly-spacing
        // @fixable
        // 'template-curly-spacing': 'error',

        // enforce spacing around the * in yield* expressions
        // https://eslint.org/docs/rules/yield-star-spacing
        // @fixable
        // 'yield-star-spacing': ['error', 'after']
    }
};
