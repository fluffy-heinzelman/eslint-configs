const { rules } = require('eslint-config-airbnb-base/rules/errors');

module.exports = {
    rules: {
        ...rules

        // Enforce “for” loop update clause moving the counter in the right direction
        // https://eslint.org/docs/rules/for-direction
        // @recommended
        // 'for-direction': 'error',

        // Enforces that a return statement is present in property getters
        // https://eslint.org/docs/rules/getter-return
        // @recommended
        // 'getter-return': ['error', { allowImplicit: true }],

        // disallow using an async function as a Promise executor
        // https://eslint.org/docs/rules/no-async-promise-executor
        // @recommended
        // 'no-async-promise-executor': 'error',

        // Disallow await inside of loops
        // https://eslint.org/docs/rules/no-await-in-loop
        // 'no-await-in-loop': 'error',

        // Disallow comparisons to negative zero
        // https://eslint.org/docs/rules/no-compare-neg-zero
        // @recommended
        // 'no-compare-neg-zero': 'error',

        // disallow assignment in conditional expressions
        // @recommended
        // 'no-cond-assign': ['error', 'always'],

        // disallow use of console
        // 'no-console': 'warn',

        // disallow use of constant expressions in conditions
        // @recommended
        // 'no-constant-condition': 'warn',

        // disallow control characters in regular expressions
        // @recommended
        // 'no-control-regex': 'error',

        // disallow use of debugger
        // @recommended
        // 'no-debugger': 'error',

        // disallow duplicate arguments in functions
        // @recommended
        // 'no-dupe-args': 'error',

        // Disallow duplicate conditions in if-else-if chains
        // https://eslint.org/docs/rules/no-dupe-else-if
        // @recommended
        // 'no-dupe-else-if': 'error',

        // disallow duplicate keys when creating object literals
        // @recommended
        // 'no-dupe-keys': 'error',

        // disallow a duplicate case label.
        // @recommended
        // 'no-duplicate-case': 'error',

        // disallow empty statements
        // @recommended
        // 'no-empty': 'error',

        // disallow the use of empty character classes in regular expressions
        // @recommended
        // 'no-empty-character-class': 'error',

        // disallow assigning to the exception in a catch block
        // @recommended
        // 'no-ex-assign': 'error',

        // disallow double-negation boolean casts in a boolean context
        // https://eslint.org/docs/rules/no-extra-boolean-cast
        // @fixable
        // @recommended
        // 'no-extra-boolean-cast': 'error',

        // disallow unnecessary parentheses
        // https://eslint.org/docs/rules/no-extra-parens
        // @fixable
        // 'no-extra-parens': ['off', 'all', {
        //     conditionalAssign: true,
        //     nestedBinaryExpressions: false,
        //     returnAssign: false,
        //     ignoreJSX: 'all', // delegate to eslint-plugin-react
        //     enforceForArrowConditionals: false,
        // }],

        // disallow unnecessary semicolons
        // @fixable
        // @recommended
        // 'no-extra-semi': 'error',

        // disallow overwriting functions written as function declarations
        // @recommended
        // 'no-func-assign': 'error',

        // disallow assigning to imported bindings
        // https://eslint.org/docs/rules/no-import-assign
        // @recommended
        // 'no-import-assign': 'off',

        // disallow function or variable declarations in nested blocks
        // @recommended
        // 'no-inner-declarations': 'error',

        // disallow invalid regular expression strings in the RegExp constructor
        // @recommended
        // 'no-invalid-regexp': 'error',

        // disallow irregular whitespace outside of strings and comments
        // @recommended
        // 'no-irregular-whitespace': 'error',

        // Disallow Number Literals That Lose Precision
        // https://eslint.org/docs/rules/no-loss-of-precision
        // 'no-loss-of-precision': 'error',

        // Disallow characters which are made with multiple code points in character class syntax
        // https://eslint.org/docs/rules/no-misleading-character-class
        // @recommended
        // 'no-misleading-character-class': 'error',

        // disallow the use of object properties of the global object (Math and JSON) as functions
        // @recommended
        // 'no-obj-calls': 'error',

        // Disallow returning values from Promise executor functions
        // https://eslint.org/docs/rules/no-promise-executor-return
        // 'no-promise-executor-return': 'error',

        // disallow use of Object.prototypes builtins directly
        // https://eslint.org/docs/rules/no-prototype-builtins
        // @recommended
        // 'no-prototype-builtins': 'error',

        // disallow multiple spaces in a regular expression literal
        // @fixable
        // @recommended
        // 'no-regex-spaces': 'error',

        // Disallow returning values from setters
        // https://eslint.org/docs/rules/no-setter-return
        // @recommended
        // 'no-setter-return': 'error',

        // disallow sparse arrays
        // @recommended
        // 'no-sparse-arrays': 'error',

        // Disallow template literal placeholder syntax in regular strings
        // https://eslint.org/docs/rules/no-template-curly-in-string
        // 'no-template-curly-in-string': 'error',

        // Avoid code that looks like two expressions but is actually one
        // https://eslint.org/docs/rules/no-unexpected-multiline
        // @recommended
        // 'no-unexpected-multiline': 'error',

        // disallow unreachable statements after a return, throw, continue, or break statement
        // @recommended
        // 'no-unreachable': 'error',

        // Disallow loops with a body that allows only one iteration
        // https://eslint.org/docs/rules/no-unreachable-loop
        // @recommended
        // 'no-unreachable-loop': ['error', {
        //     ignore: [], // WhileStatement, DoWhileStatement, ForStatement, ForInStatement, ForOfStatement
        // }],

        // disallow return/throw/break/continue inside finally blocks
        // https://eslint.org/docs/rules/no-unsafe-finally
        // 'no-unsafe-finally': 'error',

        // disallow negating the left operand of relational operators
        // https://eslint.org/docs/rules/no-unsafe-negation
        // @recommended
        // 'no-unsafe-negation': 'error',

        // disallow use of optional chaining in contexts where the undefined value is not allowed
        // https://eslint.org/docs/rules/no-unsafe-optional-chaining
        // 'no-unsafe-optional-chaining': ['error', {disallowArithmeticOperators: true}],

        // Disallow Unused Private Class Members
        // https://eslint.org/docs/rules/no-unused-private-class-members
        // TODO: enable once eslint 7 is dropped (which is semver-major)
        // 'no-unused-private-class-members': 'off',

        // Disallow useless backreferences in regular expressions
        // https://eslint.org/docs/rules/no-useless-backreference
        // 'no-useless-backreference': 'error',

        // disallow negation of the left operand of an in expression
        // deprecated in favor of no-unsafe-negation
        // 'no-negated-in-lhs': 'off',
        // Disallow assignments that can lead to race conditions due to usage of await or yield
        // https://eslint.org/docs/rules/require-atomic-updates
        // note: not enabled because it is very buggy
        // 'require-atomic-updates': 'off',

        // disallow comparisons with the value NaN
        // @recommended
        // 'use-isnan': 'error',

        // ensure JSDoc comments are valid
        // https://eslint.org/docs/rules/valid-jsdoc
        // 'valid-jsdoc': 'off',

        // enforce comparing `typeof` expressions against valid strings
        // @recommended
        // 'valid-typeof': ['error', { requireStringLiterals: true }]
    }
};
